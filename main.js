import "alpinejs";
import axios from "axios";
import { load } from "js-yaml";
import rehypeStringify from "rehype-stringify";
import remarkExtractFrontmatter from "remark-extract-frontmatter";
import remarkFrontmatter from "remark-frontmatter";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import remarkRetext from "remark-retext";
import retextEnglish from "retext-english";
import retextSmartypants from "retext-smartypants";
import unified from "unified";
import { parse as yaml } from "yaml";

const processor = unified()
  .use(remarkParse)
  .use(remarkFrontmatter)
  .use(remarkExtractFrontmatter, { yaml: load })
  .use(remarkRetext, unified().use(retextEnglish).use(retextSmartypants))
  .use(remarkRehype)
  .use(rehypeStringify);

window.loadRaw = () =>
  axios({
    url: `/${window.location.hash.substring(1)}.md`,
  })
    .then((res) =>
      res.status === 200
        ? Promise.resolve(res.data)
        : Promise.reject(new Error("no such chapter"))
    )
    .then(processor.process);
